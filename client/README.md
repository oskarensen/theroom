## Installation

```bash
$ npm install
```

## Running the app locally

```bash
# development
$ npm run start