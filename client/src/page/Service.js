import React, { useState, useEffect } from 'react';

import ItemService from './../components/ItemService';

const Service = () => {

    const [filterValue, setfilterValue] = useState("");

    const loadData = () => {
        console.log('loadData', filterValue);
    }

    const activateBonus = (id) => {
        console.log("Activate bonus", id);
    }

    useEffect(() => {
        loadData();
    }, [filterValue]);

    const resetFilter = () => {
        setfilterValue("");
    }

    const items = [
        { id: "1", name: "Sitecostructor.io", description: "Description 1", code: 'itpromocode1', state: 'NOT_ACTIVE' },
        { id: "2", name: "Appvision.com", description: "Description 2", code: 'itpromocode2', state: 'ACTIVE' },
        { id: "3", name: "Analytics.com", description: "Description 3", code: 'itpromocode3', state: 'NOT_ACTIVE' },
        { id: "4", name: "Logotype", description: "Description 4", code: 'itpromocode4', state: 'NOT_ACTIVE' },
    ];

    return (
        <div className="service">
            <h2>Services</h2>

            <div className="filterWrapper">
                <div className="text-gray f-sm">Filter</div>
                <input type="text" value={filterValue} onChange={(e) => setfilterValue(e.target.value)} />
                <div className="btn btn-primary" onClick={() => resetFilter()}>Reset</div>
            </div>

            <div className="bonusWrapper ">
                {items.map((item) => (
                    <ItemService activateBonus={activateBonus} {...item} />
                ))}
            </div>
        </div>
    )
}

export default Service;
