import React from 'react';

import logo from './../assets/image/logo.png'

function Login(props) {
    return (
        <form class="form-signin text-center">

            <img class="mb-4" src={logo} alt="" width="170" />

            <h1 class="h3 mb-3 font-weight-normal">Please sign in to The Room</h1>
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" id="inputEmail" class="form-control mb-3" placeholder="Email address" required autofocus />

            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Password" required />
            <div class="checkbox mb-3 mt-3">
                <label>
                    <input type="checkbox" value="remember-me" /> Remember me
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            <p class="mt-5 mb-3 text-muted">The Room &copy; 2021</p>
        </form>
    );
}

export default Login;