import React from 'react';

const ItemService = ({ id, name, description, code, state, activateBonus }) => {

    const copyToClipboard = (textToCopy) => {
        navigator.clipboard.writeText(textToCopy)
    }

    return (
        <div className="itemService row">
            <div className="col-md-5 col-xs-12 text-center">
                <div className="">{name}</div>
                <div className="text-gray f-sm">{description}</div>
            </div>

            <div className="col-md-2 col-xs-12 text-center">
                <div className="text-gray f-sm">PROMOCODE</div>
                <div className="promocode-input">
                    {code}
                    <span className="copyPromocode" onClick={() => { copyToClipboard(code) }}>
                        <i className="fa fa-copy" />
                    </span>
                </div>
            </div>

            <div className="col-md-5 col-xs-12 text-center">
                {state == 'ACTIVE' && <div className="mt-3 text-gray">Bonus already activated</div>}
                {state == 'NOT_ACTIVE' && <div className="btn btn-primary" onClick={() => { activateBonus(id) }}>Activate bonus</div>}
            </div>
        </div>
    )
}

export default ItemService;
