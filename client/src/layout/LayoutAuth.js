import React from "react";

import Sidebar from './Sidebar'
import Header from './Header'
import Footer from './Footer'

export const LayoutAuth = props => {

    return (
        <div className="app-wrap">
            <div className="container">
                {props.children}
            </div>
        </div>

    )
}