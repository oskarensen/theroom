import React from 'react';

import { useHistory } from "react-router-dom";

import logo from './../assets/image/logo.png'
import image from './../assets/image/image.png'

const Sidebar = () => {
    let history = useHistory();

    const logout = () => {
        console.log('Logout');
        history.push("/login");
    }

    return (
        <div id="sidebar-wrapper">
            <ul id="sidebar_menu" className="sidebar-nav">
                <li className="sidebar-brand">
                    <a id="menu-toggle" href="#">
                        <img src={logo} width="90" />
                    </a>
                </li>
            </ul>
            <ul className="sidebar-nav" id="sidebar">
                <li> <a className="cp">Promo</a></li>
                <li> <a><img src={image} /></a></li>
                <li> <a><img src={image} /></a></li>
                <li> <a><img src={image} /></a></li>
                <li> <a className="cp" onClick={() => logout()}> <i className="fa fa-sign-out" /> </a> </li>
            </ul>
        </div>
    )
}

export default Sidebar;
