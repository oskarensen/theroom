import React from "react";

import Sidebar from './Sidebar'
import Header from './Header'
import Footer from './Footer'

export const LayoutPrivate = props => {

    return (
        <div className="app-wrap container-fluid">
            <Sidebar />
            <Header />

            <div className="body">
                {props.children}
            </div>

            <Footer />
        </div>

    )
}