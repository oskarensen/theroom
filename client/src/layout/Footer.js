import React from 'react';

const Footer = () => <div className="footer">
    <div className="text-gray">The Room &copy; 2021</div>
</div>;

export default Footer;
