import React from 'react';

const Header = () => <div className="header">
    <div className="balance text-gray">
        Balance
        <div className="number">
            213 920 $
        </div>
    </div>
    <div className="balance text-gray">
        Payout
        <div className="number">
            213 920 $
        </div>
    </div>
</div>;

export default Header;
