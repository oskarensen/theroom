import React from 'react';

import { LayoutPrivate } from './layout/LayoutPrivate'
import { LayoutAuth } from './layout/LayoutAuth'

import Login from './page/Login'
import Service from './page/Service'
import PageNotFound from './page/PageNotFound'

import {
    HashRouter as Router,
    Route,
    Switch,
} from 'react-router-dom';


function OpenRoute({ component: Component, ...rest }) {
    return <Route {...rest} render={props => (<LayoutAuth><Component {...props} /> </LayoutAuth>)} />;
}

const PrivateRoute = ({ component: Component, ...rest }) => {
    // let me = loadState();
    // if (!me) return <Route {...rest} render={props => (<Redirect to="/login" />)} />

    return <Route {...rest} render={props => (<LayoutPrivate> <Component {...props} /> </LayoutPrivate>)} />
}

const routes = [
    {
        path: '/login',
        component: Login,
        route: OpenRoute
    },
    {
        exact: true,
        path: '/',
        component: Service,
        route: PrivateRoute
    },
    {
        path: '/service',
        component: Service,
        route: PrivateRoute
    },
    {
        path: '/*',
        component: PageNotFound,
        route: OpenRoute
    },
]


const App = () => {
    return (
        <Router>
            <Switch>
                {routes.map((route) => (
                    <route.route
                        exact={route.exact}
                        key={route.path}
                        path={route.path}
                        component={route.component}
                    />
                ))}
            </Switch>
        </Router>
    )
}

export default App;
