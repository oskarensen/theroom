import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';

import './assets/css/custom.css';

ReactDOM.render(<App />, document.getElementById('app'));

module.hot.accept();
